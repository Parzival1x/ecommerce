        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Navbar</title>
            <link rel="stylesheet" href="nav.css">
            <script src="nav.js"></Script>
        </head>
        <body>
            <nav class="navbar">
                <div class="nav">
                    <img src="img/logo.jpg" class="brand-logo" alt="">
                    <div class="nav-items">
                        <div class="search">
                            <input type="text" class="search-box" placeholder="search brand, product">
                            <button class="search-btn">search</button>
                        </div>
                        <a href="#"><img src="img/user.jpg" alt=""></a>
                        <a href="#"><img src="img/cart.png" alt=""></a>
                    </div>
                </div>
                <ul class="links-container">
                    <li class="link-item"><a href="#" class="link">Couch</a></li>
                    <li class="link-item"><a href="#" class="link">Bedroom</a></li>
                    <li class="link-item"><a href="#" class="link">TV unit</a></li>
                    <li class="link-item"><a href="#" class="link">Tables</a></li>
                    <li class="link-item"><a href="#" class="link">accessories</a></li>
                </ul>
                </nav>
        
        </body>
        </html>